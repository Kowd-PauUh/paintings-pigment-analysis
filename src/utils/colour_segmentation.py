from typing import Tuple

import pandas as pd
import numpy as np
from tqdm.auto import trange
import matplotlib.pyplot as plt
from skimage import color
from sklearn.cluster import KMeans


def get_segments(
    image: np.ndarray, 
    distance_between_centroids: int = 16,
    coordinates_bounding: bool = True
) -> Tuple[np.ndarray]:
    height = image.shape[0]
    width = image.shape[1]
    channels = image.shape[2]
    
    # convert RGB to Lab
    lab = color.rgb2lab(image)
    features = lab.reshape(height * width, channels)  # 3D array to 2D array
    
    # add XY coordinate feature so the clusters are coordinates-bounded
    if coordinates_bounding:
        x_coordinate = np.tile(np.arange(width), height).reshape(-1, 1)
        y_coordinate = np.tile(np.arange(height).reshape(-1, 1), width).reshape(-1, 1)
        features = np.concatenate([features, x_coordinate, y_coordinate], 1)
    
    # initial centroids coordinates
    centroids = []
    for i in range(distance_between_centroids, height, distance_between_centroids):
        for j in range(distance_between_centroids, width, distance_between_centroids):
            centroids.append(features[width * i + j, :])

    # get clusters
    kmeans = KMeans(
        n_clusters=len(centroids), 
        init=centroids, 
        random_state=42, 
        n_init=1
    )
    kmeans.fit(features)
    
    labels = np.reshape(kmeans.labels_, (height, width))
    centroids = kmeans.cluster_centers_
    return labels, centroids


def contour_mask(array: np.ndarray) -> np.ndarray:
    # create shifted arrays
    shifted_right = np.roll(array, shift=-1, axis=1)
    shifted_down = np.roll(array, shift=-1, axis=0)

    # compare shifted arrays with the original one, thus finding contours
    horizontal_diff = array[:, :-1] != shifted_right[:, :-1]
    vertical_diff = array[:-1, :] != shifted_down[:-1, :]

    # add missing row and column
    horizontal_diff = np.hstack((horizontal_diff, np.zeros((array.shape[0], 1), dtype=bool)))
    vertical_diff = np.vstack((vertical_diff, np.zeros((1, array.shape[1]), dtype=bool)))

    contour_mask = np.logical_or(horizontal_diff, vertical_diff)
    return contour_mask


def draw_contours(
    image: np.ndarray,
    labels: np.ndarray
) -> np.ndarray:
    mask = contour_mask(labels)
    image = np.copy(image)
    
    # black contours
    for channel in range(image.shape[2]):
        image[:, :, channel][mask] = 0
    return image
