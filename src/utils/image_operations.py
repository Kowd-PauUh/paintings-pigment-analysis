from typing import List

from skimage.util import img_as_float
import numpy as np
import cv2


def get_image(path: str) -> np.ndarray:
    img = cv2.imread(path, cv2.IMREAD_COLOR)
    return img_as_float(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


def split_image(image: np.ndarray, sample_size: int = 32) -> List[np.ndarray]:
    height, width = image.shape[:2]
    samples = []
    for y in range(0, height, sample_size):
        for x in range(0, width, sample_size):
            sample = image[y:y+sample_size, x:x+sample_size]
            samples.append(sample)
    return samples
