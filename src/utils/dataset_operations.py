from typing import Callable
import warnings
from pathlib import Path

import pandas as pd
from tqdm.auto import tqdm

from .image_operations import get_image, split_image


def load_dataset(
    images_dir: str, 
    target_name: str = 'label',
    sample_size: int | None = None,
    features_extractor: Callable | None = None
) -> pd.DataFrame:
    # get patches paths
    images_dir = Path(images_dir)
    df = pd.DataFrame({'img_path': [path for path in images_dir.rglob('*.jpg')]})

    # load images
    tqdm.pandas(desc='Loading images')
    df[target_name] = df['img_path'].apply(lambda path: path.stem)
    df['image'] = df['img_path'].progress_apply(lambda path: get_image(path.as_posix()))
    df.drop('img_path', axis=1, inplace=True)

    # split images
    if sample_size is not None:
        tqdm.pandas(desc='Splitting images')
        df['sample'] = df['image'].progress_apply(
            lambda image: split_image(
                image, 
                sample_size=sample_size
            )
        )
        df.drop('image', axis=1, inplace=True)
        df = df.explode('sample')

    # extract features
    if features_extractor is not None:
        warnings.simplefilter("ignore", UserWarning)

        tqdm.pandas(desc='Features extraction')
        features = df['sample'].progress_apply(lambda sample: features_extractor(sample))

        warnings.resetwarnings()
        df = pd.concat([df, features], axis=1)
        df.drop('sample', axis=1, inplace=True)

    tqdm.pandas()
    return df
