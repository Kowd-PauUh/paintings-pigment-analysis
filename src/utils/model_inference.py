from typing import Callable

import numpy as np

from src.utils.image_operations import get_image
from src.utils.colour_segmentation import get_segments


def inference(image: np.ndarray, model, features_extractor: Callable):    
    # get segments from image
    segments, centroids = get_segments(
        image=image,
        distance_between_centroids=30,
        coordinates_bounding=True
    )
    centroids = centroids[:, -2:]

    # calculate features per segment
    features = []
    for segment_label in np.unique(segments):
        mask = segments != segment_label
        segment = np.copy(image)
        segment[mask, :] = [0, 0, 0]
        features.append(features_extractor(segment))

    predictions = model.predict(features)

    return image, segments, centroids, predictions
