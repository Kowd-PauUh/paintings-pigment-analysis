from skimage.feature import graycomatrix, graycoprops
from skimage.color import rgb2gray
from skimage import feature
from sklearn.preprocessing import normalize
import numpy as np
import pandas as pd


def extract_color_features(
    image: np.ndarray, 
    hist: bool = True
) -> pd.Series:
    """Extracts color features and optionally hist features
    
    Parameters
    ----------
    image : numpy.ndarray
        Image as numpy array.
    hist : bool
        Boolean flag whether to extract hist features from image.
    
    Returns
    -------
    pandas.Series
        Extracted features as pandas.Series.
    """
    flattened_img = image.reshape(-1, 3)
    
    # color features
    mean_color = np.mean(flattened_img, axis=0)
    std_color = np.std(flattened_img, axis=0)
    
    hist_r, hist_g, hist_b = [], [], []
    if hist:
        hist_r = np.histogram(flattened_img[:, 0], bins=256, range=[0, 256])[0]
        hist_g = np.histogram(flattened_img[:, 1], bins=256, range=[0, 256])[0]
        hist_b = np.histogram(flattened_img[:, 2], bins=256, range=[0, 256])[0]
    
    color_features = pd.Series(
        np.concatenate([mean_color, std_color, hist_r, hist_g, hist_b]), 
        index=['mean_red', 'mean_green', 'mean_blue', 'std_red', 'std_green', 'std_blue'] + \
              [f'hist_red_{i}' for i in range(256 if hist else 0)] + \
              [f'hist_green_{i}' for i in range(256 if hist else 0)] + \
              [f'hist_blue_{i}' for i in range(256 if hist else 0)]
    )    
    return color_features


def extract_textural_features(
    image: np.ndarray, 
    lbp: bool = True, 
    normalize_features: bool = False
) -> pd.Series:
    """Extracts textural features and optionally lbp (local binary pattern) features
    
    Parameters
    ----------
    image : numpy.ndarray
        Image as numpy array.
    lbp : bool
        Boolean flag whether to extract lbp features from image.
    
    Returns
    -------
    pandas.Series
        Extracted features as pandas.Series.
    """
    gray_img = rgb2gray(image)
    gray_img_uint = (gray_img * 255).astype(np.uint8)
    glcm = graycomatrix(gray_img_uint, [1], [0], 256, symmetric=True, normed=True)
    
    # texture features
    contrast = graycoprops(glcm, 'contrast').ravel()
    dissimilarity = graycoprops(glcm, 'dissimilarity').ravel()
    homogeneity = graycoprops(glcm, 'homogeneity').ravel()
    energy = graycoprops(glcm, 'energy').ravel()
    correlation = graycoprops(glcm, 'correlation').ravel()
    
    if lbp:
        lbp = feature.local_binary_pattern(gray_img, 8, 1, method='uniform')
        lbp_hist, _ = np.histogram(lbp, bins=np.arange(0, 8 + 3), range=(0, 8 + 2))
    else:
        lbp_hist = np.array([])
    
    texture_features = np.concatenate([contrast, dissimilarity, homogeneity, energy, correlation, lbp_hist])
    if normalize_features:
        texture_features = normalize([texture_features])[0]
        
    texture_features = pd.Series(
        texture_features, 
        index=['contrast', 'dissimilarity', 'homogeneity', 'energy', 'correlation'] + \
              [f'lbp_{i}' for i in range(len(lbp_hist))]
    )
    
    return texture_features


def extract_all_features(
    image: np.ndarray,
    hist: bool,
    lbp: bool,
    normalize_textural_features: bool
) -> pd.Series:
    """Extracts both textural and color features from image by given path.
    
    Parameters
    ----------
    image : numpy.ndarray
        Image as numpy array.
    hist : bool
        Boolean flag whether to extract hist features from image.
    lbp : bool
        Boolean flag whether to extract lbp features from image.
    normalize_textural_features : bool
        Boolean flag whether to perform normalization of textural features.
    
    Returns
    -------
    pandas.Series
        Extracted features as pandas.Series.
    """
    color_features = extract_color_features(
        image, 
        hist=hist
    )
    texture_features = extract_textural_features(
        image, 
        lbp=lbp, 
        normalize_features=normalize_textural_features
    )
    features = pd.concat([color_features, texture_features])
    
    return features
