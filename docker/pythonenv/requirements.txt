# general
numpy==1.26.4
pandas==2.2.2
tqdm==4.66.4

# image processing
albumentations==1.4.6
opencv-python-headless==4.9.0.80

# modelling
scikit-learn==1.4.2

# data exploration
jupyter==1.0.0
seaborn==0.13.2