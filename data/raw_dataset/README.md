`catalog.pdf` is the original source from which the training dataset was created. The catalog presents images of pigment patches and their names and is firstly introduced in  _"Hyperspectral Pigment Dataset"_ [1].
All credit goes to Hilda Deborah.

[1] H. Deborah. 2022. Hyperspectral Pigment Dataset. 12th Workshop on Hyperspectral Imaging and Signal Processing: Evolution in Remote Sensing (WHISPERS). 